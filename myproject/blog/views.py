from django.views.generic import ListView, DetailView
from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404
from .models import Article, Category
from account.mixins import AuthorAccessMixin
from account.models import User
from django.db.models import Q


# Create your views here.
# We can use of published() manager instead of Article.objects.filter(status='p')
# def index(request, page=1):
#     article_list = Article.objects.published()
#     paginator = Paginator(article_list, 2)  # Show 2 contacts per page.
#     # page = request.GET.get('page')
#     page_obj = paginator.get_page(page)
#     context = {
#         'articles': page_obj,
#         # 'categories': Category.objects.filter(status=True)
#         # instead of this command we use of templatetags
#     }
#     return render(request, 'blog/article_list.html', context)


# Changed functional index view to class ArticleListView
class ArticleListView(ListView):
    # model = Article    # We do not use of model because we do not want all articles.
    # context_object_name = 'articles'   # We can use of this or use of object_list in html file
    queryset = Article.objects.published()
    # template_name = 'blog/article_list.html'   # We can determine template name or its default name (article_list)
    paginate_by = 2   # For paginating we must use of default content page_obj instead of articles


# We can use of published() manager instead of (Article, slug=slug, status='p')
# def article_detail(request, slug):
#     context = {
#         'article': get_object_or_404(Article.objects.published(), slug=slug)
#     }
#     return render(request, 'blog/article_detail.html', context)


# Changed functional article_detail view to class ArticleDetailView
class ArticleDetailView(DetailView):
    def get_object(self, queryset=None):
        slug = self.kwargs.get('slug')
        article = get_object_or_404(Article.objects.published(), slug=slug)

        ip_address = self.request.user.ip_address
        if ip_address not in article.hits.all():
            article.hits.add(ip_address)

        return article


class ArticlePreview(AuthorAccessMixin, DetailView):
    def get_object(self, queryset=None):
        pk = self.kwargs.get('pk')
        return get_object_or_404(Article, pk=pk)


# def category_list(request, slug, page=1):
#     category = get_object_or_404(Category, slug=slug, status=True)
#     article_list = category.articles.published()
#     paginator = Paginator(article_list, 2)  # Show 2 contacts per page.
#     page_obj = paginator.get_page(page)
#     context = {
#         'category': category,
#         'articles': page_obj
#     }
#     return render(request, 'blog/list.html', context)


# Changed functional category_list view to class CategoryListView
class CategoryListView(ListView):
    paginate_by = 2
    template_name = 'blog/category_list.html'

    def get_queryset(self):
        global category
        slug = self.kwargs.get('slug')
        category = get_object_or_404(Category.objects.active(), slug=slug)
        return category.articles.published()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = category
        return context


class AuthorListView(ListView):
    paginate_by = 2
    template_name = 'blog/author_list.html'

    def get_queryset(self):
        global author
        username = self.kwargs.get('username')
        author = get_object_or_404(User, username=username)
        return author.articles.published()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['author'] = author
        return context


class SearchList(ListView):
    paginate_by = 2
    template_name = 'blog/search_list.html'

    def get_queryset(self):
        search = self.request.GET.get('q')
        return Article.objects.filter(Q(description__icontains=search) | Q(title__icontains=search))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('q')
        return context
