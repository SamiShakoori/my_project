from django.contrib import admin
from .models import Article, Category, IPAddress
from django.contrib import messages
from django.utils.translation import ngettext
from account.models import User


# Register your models here.
# Actions in admin panel
# admin.site.disable_action('delete_selected')   # For disabling some actions
def make_published(modeladmin, request, queryset):
    updated = queryset.update(status='p')
    modeladmin.message_user(request, ngettext(
        '%d مقاله منتشر شد.',
        '%d مقاله منتشر شدند.',
        updated,
    ) % updated, messages.SUCCESS)

make_published.short_description = 'انتشار مقالات انتخاب شده'


def make_drafted(modeladmin, request, queryset):
    updated = queryset.update(status='d')
    modeladmin.message_user(request, ngettext(
        '%d مقاله پیش نویس شد.',
        '%d مقاله پیش نویس شدند.',
        updated,
    ) % updated, messages.SUCCESS)

make_drafted.short_description = 'پیش نویس مقالات انتخاب شده'


# Displaying some icon in admin panel
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('position', 'title', 'slug', 'parent', 'status')
    list_filter = (['status'])
    search_fields = ('title', 'description')
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Category, CategoryAdmin)


# Displaying some icon in admin panel
class ArticleAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "author":
            kwargs["queryset"] = User.objects.filter(is_staff=True)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    list_display = ('title', 'thumbnail_tag', 'slug', 'author', 'publish', 'is_special', 'status', 'category_to_str')
    list_filter = ('publish', 'status', 'author')
    search_fields = ('title', 'description')
    prepopulated_fields = {'slug': ('title',)}
    ordering = ['-status', '-publish']
    actions = [make_published, make_drafted]

    # This def for displaying category in admin panel
    # We can not use category in list_display because it is a list that list_display be able not a list. As result
    # we can use of join method. Point - we can def category_to_str in article model like def __str__ .
    # We can use of obj.category_published() instead of obj.category.all()
    # def category_to_str(self, obj):
    #     return '-'.join([category.title for category in obj.category_published()])
    # category_to_str.short_description = 'دسته بندی'
    # for we can use of this def in admin.py and template we must this def transfer to model.py


admin.site.register(Article, ArticleAdmin)
admin.site.register(IPAddress)
