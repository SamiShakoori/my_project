from django.urls import path
from .views import ArticleListView, ArticleDetailView, CategoryListView, AuthorListView, ArticlePreview, SearchList


app_name = 'blog'
urlpatterns = [
    path('', ArticleListView.as_view(), name='index'),
    path('page/<int:page>', ArticleListView.as_view(), name='index'),  # this url for pagination
    path('detail/<slug:slug>/', ArticleDetailView.as_view(), name='article_detail'),
    path('preview/<int:pk>/', ArticlePreview.as_view(), name='article-preview'),
    path('category/<slug:slug>/', CategoryListView.as_view(), name='category_list'),
    path('category/<slug:slug>/page/<int:page>/', CategoryListView.as_view(), name='category_list'),
    path('author/<slug:username>/', AuthorListView.as_view(), name='author_list'),
    path('author/<slug:username>/page/<int:page>/', AuthorListView.as_view(), name='author_list'),
    path('search/', SearchList.as_view(), name='search'),
    path('search/page/<int:page>/', SearchList.as_view(), name='search'),
]
