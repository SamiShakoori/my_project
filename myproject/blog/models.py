from django.db import models
from django.urls import reverse
from account.models import User
from django.utils.html import format_html
from django.utils import timezone
from django.contrib.contenttypes.fields import GenericRelation
from comment.models import Comment


# Create your models here.
# My managers
# This is just for displaying published articles
class ArticleManager(models.Manager):
    def published(self):
        return self.filter(status='p')


# This just for displaying active categories
class CategoryManger(models.Manager):
    def active(self):
        return self.filter(status=True)


# Create Category model
class IPAddress(models.Model):
    ip_address = models.GenericIPAddressField(verbose_name='آدرس آیپی')


class Category(models.Model):
    parent = models.ForeignKey('self', default=None, null=True, blank=True, on_delete=models.SET_NULL, related_name='children', verbose_name='زیر دسته')
    title = models.CharField(max_length=200, verbose_name='عنوان دسته بندی')
    slug = models.CharField(max_length=100, unique=True, verbose_name='آدرس دسته بندی')
    status = models.BooleanField(default=True, verbose_name='آیا دسته بندی نمایش داده شود؟')
    position = models.IntegerField(verbose_name='وضعیت')

    # class meta for making persian language
    class Meta:
        verbose_name = 'دسته بندی '
        verbose_name_plural = 'دسته بندی ها'
        ordering = ['parent__id', 'position']   # We use of parent__id for displaying first parent then children in admin/blog/category.

    # def __str__ for returning objects to string
    def __str__(self):
        return self.title

    objects = CategoryManger()


# Create Article model
class Article(models.Model):
    STATUS_CHOICES = (
        ('d', 'پیش نویس'),              # draft
        ('p', 'منتشر شده'),             # published
        ('i', 'در حال بررسی'),          # investigation
        ('b', 'برگشت داده شده'),        # back
    )
    author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name='articles', verbose_name='نویسنده')
    title = models.CharField(max_length=200, verbose_name='عنوان مقاله')
    slug = models.CharField(max_length=100, unique=True, verbose_name='آدرس مقاله')
    # related_name for using in html file
    category = models.ManyToManyField(Category, verbose_name='دسته بندی', related_name='articles')
    description = models.TextField(verbose_name='محتوا')
    thumbnail = models.ImageField(upload_to='images', verbose_name='تصویر مقاله')
    publish = models.DateTimeField(default=timezone.now, verbose_name='زمان انتشار')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    is_special = models.BooleanField(default=False, verbose_name='مقاله ویژه')
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, verbose_name='وضعیت')
    # This is for comment section.
    comments = GenericRelation(Comment)
    # This relation many to many is for IPAddress
    hits = models.ManyToManyField(IPAddress, through='ArticleHit', blank=True, related_name='hits', verbose_name='بازدیدها')

    # class meta for making persian language
    class Meta:
        verbose_name = 'مقاله'
        verbose_name_plural = 'مقاله ها'
        ordering = ['-publish']

    # def __str__ for returning objects to string
    def __str__(self):
        return self.title

    # we created this def for when we send a article, that it redirects us to index page
    def get_absolute_url(self):
        return reverse('account:index')

    # this is just for displaying published category
    def category_published(self):
        return self.category.filter(status=True)

    # After we wrote manager we must adding to objects
    objects = ArticleManager()

    # For showing thumbnail in admin panel
    def thumbnail_tag(self):
        return format_html("<img src='{}' width=100 height=70 style='border-radius: 5px' ".format(self.thumbnail.url))
    thumbnail_tag.short_description = 'عکس'

    # This def for displaying category in admin panel
    # We can not use category in list_display because it is a list that list_display be able not a list. As result
    # we can use of join method. Point - we can def category_to_str in article model like def __str__ .
    # We can use of obj.category_published() instead of obj.category.all()
    def category_to_str(self):
        return '-'.join([category.title for category in self.category_published()])
    category_to_str.short_description = 'دسته بندی'
    # Important point : we do not need obj object (def category_to_str(self, obj)) here for this reason delete obj (def category_to_str(self))


class ArticleHit(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    ip_address = models.ForeignKey(IPAddress, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)